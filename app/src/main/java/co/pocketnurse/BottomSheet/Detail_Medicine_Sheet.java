package co.pocketnurse.BottomSheet;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import co.pocketnurse.Adapter.SideEffectsAdapter;
import co.pocketnurse.R;

import java.util.ArrayList;
import java.util.Collections;

public class Detail_Medicine_Sheet extends BottomSheetDialogFragment {


    ArrayList<String> list = new ArrayList<>();
    ArrayList<String> arrayList = new ArrayList<>();
    private SideEffectsAdapter mAdapter;
    RecyclerView recyclerView;
    TextView medName;
    Button button;
    SharedPreferences preferences;
    String name;

    public Detail_Medicine_Sheet(String name) {

        this.name = name;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.bottomsheet_medicine_detail, container, false);


        recyclerView = v.findViewById(R.id.recyclerView_sideEffects);
        button = v.findViewById(R.id.submit_btn);
        medName = v.findViewById(R.id.medicine_name);
        medName.setText(name);
        preferences = PreferenceManager.getDefaultSharedPreferences(getContext());

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitSideEffects();
            }
        });

        setupList();
        showList();


        return v;
    }


    public void submitSideEffects(){


        String listString = TextUtils.join(",", mAdapter.returnList());
        preferences.edit().putString("selectedSideEffects", listString).apply();
        dismiss();

    }
    public void setupList(){

        list.add("Constipation");
        list.add("Skin rash or dermatitis");
        list.add("Diarrhea");
        list.add("Dizziness");
        list.add("Drowsiness");
        list.add("Dry mouth");
        list.add("Headache");
        list.add("Nausea");
        list.add("Insomnia");
        list.add("Hair fall");
        list.add("Itching");
        Collections.shuffle(list);

        for (int i = 0; i<6; i++){

            arrayList.add(list.get(i));

        }

    }
    public void showList(){

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mAdapter = new SideEffectsAdapter(getContext(), arrayList);
        recyclerView.setAdapter(mAdapter);

    }
}
