package co.pocketnurse.Urinalysis;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import co.pocketnurse.R;
import co.pocketnurse.databinding.ActivityUninalysisMainBinding;

public class UrinalysisMainActivity extends AppCompatActivity {

    ActivityUninalysisMainBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityUninalysisMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);


        getSupportFragmentManager().beginTransaction().replace(R.id.frame_container,
                new ScreeningColorFragment()).commit();

    }
}