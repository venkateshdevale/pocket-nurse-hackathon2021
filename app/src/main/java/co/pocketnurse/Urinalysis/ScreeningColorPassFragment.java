package co.pocketnurse.Urinalysis;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import co.pocketnurse.R;


public class ScreeningColorPassFragment extends Fragment implements View.OnClickListener {

    TextView leu, nit, uro, pro, ph, blood, spe, ketone, bil, glu;
    TextView leux, nitx, urox, prox, phx, bloodx, spex, ketonex, bilx, glux;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Please note the third parameter should be false, otherwise a java.lang.IllegalStateException maybe thrown.
        final View retView = inflater.inflate(R.layout.fragment_screening_colorimeter_pass, container, false);

        retView.findViewById(R.id.capture).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent4 = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://api.whatsapp.com/send?phone=917259926494&text=&source=&data="));
                startActivity(browserIntent4);
            }
        });

        leu = retView.findViewById(R.id.leu);
        nit = retView.findViewById(R.id.nit);
        uro = retView.findViewById(R.id.uro);
        pro = retView.findViewById(R.id.pro);
        ph = retView.findViewById(R.id.ph);
        blood = retView.findViewById(R.id.blood);
        spe = retView.findViewById(R.id.spe);
        ketone = retView.findViewById(R.id.ketone);
        bil = retView.findViewById(R.id.bil);
        glu = retView.findViewById(R.id.glu);

        leux = retView.findViewById(R.id.leu1);
        nitx = retView.findViewById(R.id.nitx);
        urox = retView.findViewById(R.id.urox);
        prox = retView.findViewById(R.id.prox);
        phx = retView.findViewById(R.id.phx);
        bloodx = retView.findViewById(R.id.bloodx);
        spex = retView.findViewById(R.id.spex);
        ketonex = retView.findViewById(R.id.ketonex);
        bilx = retView.findViewById(R.id.bilx);
        glux = retView.findViewById(R.id.glux);

        retView.findViewById(R.id.u1).setOnClickListener(this);
        retView.findViewById(R.id.u2).setOnClickListener(this);
        retView.findViewById(R.id.u3).setOnClickListener(this);
        retView.findViewById(R.id.u4).setOnClickListener(this);
        retView.findViewById(R.id.u5).setOnClickListener(this);

        retView.findViewById(R.id.n1).setOnClickListener(this);
        retView.findViewById(R.id.n2).setOnClickListener(this);
        retView.findViewById(R.id.n3).setOnClickListener(this);

        retView.findViewById(R.id.uro1).setOnClickListener(this);
        retView.findViewById(R.id.uro2).setOnClickListener(this);
        retView.findViewById(R.id.uro3).setOnClickListener(this);
        retView.findViewById(R.id.uro4).setOnClickListener(this);
        retView.findViewById(R.id.uro5).setOnClickListener(this);

        retView.findViewById(R.id.pro1).setOnClickListener(this);
        retView.findViewById(R.id.pro2).setOnClickListener(this);
        retView.findViewById(R.id.pro3).setOnClickListener(this);
        retView.findViewById(R.id.pro4).setOnClickListener(this);
        retView.findViewById(R.id.pro5).setOnClickListener(this);
        retView.findViewById(R.id.pro6).setOnClickListener(this);

        retView.findViewById(R.id.ph1).setOnClickListener(this);
        retView.findViewById(R.id.ph2).setOnClickListener(this);
        retView.findViewById(R.id.ph3).setOnClickListener(this);
        retView.findViewById(R.id.ph4).setOnClickListener(this);
        retView.findViewById(R.id.ph5).setOnClickListener(this);
        retView.findViewById(R.id.ph6).setOnClickListener(this);
        retView.findViewById(R.id.ph7).setOnClickListener(this);

        retView.findViewById(R.id.blood1).setOnClickListener(this);
        retView.findViewById(R.id.blood2).setOnClickListener(this);
        retView.findViewById(R.id.blood3).setOnClickListener(this);
        retView.findViewById(R.id.blood4).setOnClickListener(this);
        retView.findViewById(R.id.blood5).setOnClickListener(this);
        retView.findViewById(R.id.blood6).setOnClickListener(this);

        retView.findViewById(R.id.spe1).setOnClickListener(this);
        retView.findViewById(R.id.spe2).setOnClickListener(this);
        retView.findViewById(R.id.spe3).setOnClickListener(this);
        retView.findViewById(R.id.spe4).setOnClickListener(this);
        retView.findViewById(R.id.spe5).setOnClickListener(this);
        retView.findViewById(R.id.spe6).setOnClickListener(this);
        retView.findViewById(R.id.spe7).setOnClickListener(this);

        retView.findViewById(R.id.ketone1).setOnClickListener(this);
        retView.findViewById(R.id.ketone2).setOnClickListener(this);
        retView.findViewById(R.id.ketone3).setOnClickListener(this);
        retView.findViewById(R.id.ketone4).setOnClickListener(this);
        retView.findViewById(R.id.ketone5).setOnClickListener(this);
        retView.findViewById(R.id.ketone6).setOnClickListener(this);

        retView.findViewById(R.id.bil1).setOnClickListener(this);
        retView.findViewById(R.id.bil2).setOnClickListener(this);
        retView.findViewById(R.id.bil3).setOnClickListener(this);
        retView.findViewById(R.id.bil4).setOnClickListener(this);

        retView.findViewById(R.id.glu1).setOnClickListener(this);
        retView.findViewById(R.id.glu2).setOnClickListener(this);
        retView.findViewById(R.id.glu3).setOnClickListener(this);
        retView.findViewById(R.id.glu4).setOnClickListener(this);
        retView.findViewById(R.id.glu5).setOnClickListener(this);
        retView.findViewById(R.id.glu6).setOnClickListener(this);


        leu.setText("Neg");
        leux.setVisibility(View.GONE);

        nit.setText("Neg");
        nitx.setVisibility(View.GONE);

        uro.setText("3.2");
        urox.setVisibility(View.VISIBLE);
        urox.setText("Normal");

        pro.setText("Neg");
        prox.setVisibility(View.GONE);
        prox.setText("High protein - Signs of Kidney Abnormalities");

        ph.setText("5.0");

        blood.setText("Neg");
        bloodx.setVisibility(View.VISIBLE);
        bloodx.setText("All good.");

        spe.setText("1.000");
        spex.setVisibility(View.VISIBLE);
        spex.setText("All good.");

        ketone.setText("Neg");
        ketonex.setVisibility(View.GONE);

        bil.setText("Neg");
        bilx.setVisibility(View.VISIBLE);
        bilx.setText("All good.");

        glu.setText("Neg");
        glux.setVisibility(View.VISIBLE);
        glux.setText("No glucose in urine.");

        return retView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.u1:
                leu.setText("Neg");
                leux.setVisibility(View.GONE);
                break;
            case R.id.u2:
                leu.setText("15 - Traces");
                leux.setVisibility(View.GONE);
                break;
            case R.id.u3:
                leu.setText("70 - Small");
                leux.setVisibility(View.VISIBLE);
                leux.setText("Signs of Mild Infection");
                break;
            case R.id.u4:
                leu.setText("125 - Moderate");
                leux.setVisibility(View.VISIBLE);
                leux.setText("Signs of Moderate Infection");
                break;
            case R.id.u5:
                leu.setText("500 - Large");
                leux.setVisibility(View.VISIBLE);
                leux.setText("Signs of High Infection");
                break;

            case R.id.n1:
                nit.setText("Neg");
                nitx.setVisibility(View.GONE);
                break;
            case R.id.n2:
                nit.setText("Positive");
                nitx.setVisibility(View.VISIBLE);
                nitx.setText("Signs of Infection - Correlate with Leukocytes Value");
                break;
            case R.id.n3:
                nit.setText("Positive");
                nitx.setVisibility(View.VISIBLE);
                nitx.setText("Signs of Infection - Correlate with Leukocytes Value");
                break;

            case R.id.uro1:
                uro.setText("3.2");
                urox.setVisibility(View.VISIBLE);
                urox.setText("Normal");
                break;
            case R.id.uro2:
                uro.setText("16");
                urox.setVisibility(View.VISIBLE);
                urox.setText("Normal");
                break;
            case R.id.uro3:
                urox.setVisibility(View.VISIBLE);
                urox.setText("Check Liver Function");
                uro.setText("32");
                break;
            case R.id.uro4:
                uro.setText("64");
                urox.setVisibility(View.VISIBLE);
                urox.setText("Check Liver Function");
                break;
            case R.id.uro5:
                uro.setText("128");
                urox.setVisibility(View.VISIBLE);
                urox.setText("Check Liver Function");
                break;

            case R.id.pro1:
                pro.setText("Neg");
                prox.setVisibility(View.GONE);
                prox.setText("High protein - Signs of Kidney Abnormalities");
                break;
            case R.id.pro2:
                pro.setText("Traces");
                prox.setVisibility(View.VISIBLE);
                prox.setText("All good.");
                break;
            case R.id.pro3:
                pro.setText("0.3");
                prox.setVisibility(View.VISIBLE);
                prox.setText("Presence of small protein in urine.");
                break;
            case R.id.pro4:
                pro.setText("1.0");
                prox.setVisibility(View.VISIBLE);
                prox.setText("Check your kidney functioning.");
                break;
            case R.id.pro5:
                pro.setText("3.0");
                prox.setVisibility(View.VISIBLE);
                prox.setText("High protein - Signs of Kidney Abnormalities. Consult nephrologiest immediately.");
                break;
            case R.id.pro6:
                pro.setText(">20.0");
                prox.setVisibility(View.VISIBLE);
                prox.setText("High protein - Signs of Kidney Abnormalities. Consult nephrologiest immediately.");
                break;

            case R.id.ph1:
                ph.setText("5.0");
                break;
            case R.id.ph2:
                ph.setText("6.0");
                break;
            case R.id.ph3:
                ph.setText("6.5");
                break;
            case R.id.ph4:
                ph.setText("7.0");
                break;
            case R.id.ph5:
                ph.setText("7.5");
                break;
            case R.id.ph6:
                ph.setText("8.0");
                break;
            case R.id.ph7:
                ph.setText("8.5");
                break;

            case R.id.blood1:
                blood.setText("Neg");
                bloodx.setVisibility(View.VISIBLE);
                bloodx.setText("All good.");
                break;
            case R.id.blood2:
                blood.setText("Traces");
                bloodx.setVisibility(View.VISIBLE);
                bloodx.setText("Repeat test again after 3 hours.");
                break;
            case R.id.blood3:
                blood.setText("10");
                bloodx.setVisibility(View.VISIBLE);
                bloodx.setText("Traces");
                break;
            case R.id.blood4:
                blood.setText("25 - Small");
                bloodx.setVisibility(View.VISIBLE);
                bloodx.setText("Repeat test again after next 12 hours.");
                break;
            case R.id.blood5:
                blood.setText("80 - Moderate");
                bloodx.setVisibility(View.VISIBLE);
                bloodx.setText("Consult urinologist immediately.");
                break;
            case R.id.blood6:
                blood.setText("200 - Large");
                bloodx.setVisibility(View.VISIBLE);
                bloodx.setText("Consult urinologist immediately.");
                break;

            case R.id.spe1:
                spe.setText("1.000");
                spex.setVisibility(View.VISIBLE);
                spex.setText("All good.");
                break;
            case R.id.spe2:
                spe.setText("1.005");
                spex.setVisibility(View.VISIBLE);
                spex.setText("All good.");
                break;
            case R.id.spe3:
                spe.setText("1.010");
                spex.setVisibility(View.VISIBLE);
                spex.setText("Consume more water. You are dehydrated.");
                break;
            case R.id.spe4:
                spe.setText("1.015");
                spex.setVisibility(View.VISIBLE);
                spex.setText("You seem to be dehydrated. Consume 4 liters of water and test again in next 24 hours.");
                break;
            case R.id.spe5:
                spe.setText("1.020");
                spex.setVisibility(View.VISIBLE);
                spex.setText("Check your GFR value and consult doctor.");
                break;
            case R.id.spe6:
                spe.setText("1.025");
                spex.setVisibility(View.VISIBLE);
                spex.setText("Consult nephrologist immediately.");
                break;
            case R.id.spe7:
                spe.setText("1.030");
                spex.setVisibility(View.VISIBLE);
                spex.setText("Consult nephrologist immediately.");
                break;

            case R.id.ketone1:
                ketone.setText("Neg");
                ketonex.setVisibility(View.GONE);
                break;
            case R.id.ketone2:
                ketone.setText("0.5 - Traces");
                ketonex.setVisibility(View.VISIBLE);
                ketonex.setText("All good.");
                break;
            case R.id.ketone3:
                ketone.setText("1.5 - Small ");
                ketonex.setVisibility(View.VISIBLE);
                ketonex.setText("Are you on ketodiet?");
                break;
            case R.id.ketone4:
                ketone.setText("4.0 - Moderate");
                ketonex.setVisibility(View.VISIBLE);
                ketonex.setText("Eat enough carbs if you are not on ketodiet.");
                break;
            case R.id.ketone5:
                ketone.setText("8.0");
                ketonex.setVisibility(View.VISIBLE);
                ketonex.setText("Dangerous for diabetes patients. If you are diabetic, contact your diabetologist immediately.");
                break;
            case R.id.ketone6:
                ketone.setText("16");
                ketonex.setVisibility(View.VISIBLE);
                ketonex.setText("Above dangerous levels. Contact physician.");
                break;

            case R.id.bil1:
                bil.setText("Neg");
                bilx.setVisibility(View.VISIBLE);
                bilx.setText("All good.");
                break;
            case R.id.bil2:
                bil.setText("15 - Small");
                bilx.setVisibility(View.VISIBLE);
                bilx.setText("All good.");
                break;
            case R.id.bil3:
                bil.setText("50 - Moderate");
                bilx.setVisibility(View.VISIBLE);
                bilx.setText("Consult endocrynologist immediately. Danger.");
                break;
            case R.id.bil4:
                bil.setText("100 - Large");
                bilx.setVisibility(View.VISIBLE);
                bilx.setText("Consult endocrynologist immediately. Danger.");
                break;

            case R.id.glu1:
                glu.setText("Neg");
                glux.setVisibility(View.VISIBLE);
                glux.setText("No glucose in urine.");
                break;
            case R.id.glu2:
                glu.setText("Traces");
                glux.setVisibility(View.VISIBLE);
                glux.setText("Mild glucose in urine. Check in glucometer.");
                break;
            case R.id.glu3:
                glu.setText("15");
                glux.setVisibility(View.VISIBLE);
                glux.setText("Moderate glucose in urine. Check in glucometer if you are diabetic.");
                break;
            case R.id.glu4:
                glu.setText("30");
                glux.setVisibility(View.VISIBLE);
                glux.setText("Moderate glucose in urine. Check in glucometer if you are diabetic.");
                break;
            case R.id.glu5:
                glu.setText("60");
                glux.setVisibility(View.VISIBLE);
                glux.setText("High glucose in urine. Check glucose in blood and consult doctor.");
                break;
            case R.id.glu6:
                glu.setText("110");
                glux.setVisibility(View.VISIBLE);
                glux.setText("High glucose in urine. Check glucose in blood and consult doctor.");
                break;
        }
    }
}
