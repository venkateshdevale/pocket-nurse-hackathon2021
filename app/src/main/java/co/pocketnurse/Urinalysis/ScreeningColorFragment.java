package co.pocketnurse.Urinalysis;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import co.pocketnurse.API.RetrofitAPI;
import co.pocketnurse.API.RetrofitBuilder;
import co.pocketnurse.R;

import co.pocketnurse.managers.Constants;
import io.fotoapparat.Fotoapparat;
import io.fotoapparat.log.LoggersKt;
import io.fotoapparat.parameter.ScaleType;
import io.fotoapparat.result.PhotoResult;
import io.fotoapparat.selector.FlashSelectorsKt;
import io.fotoapparat.selector.FocusModeSelectorsKt;
import io.fotoapparat.selector.LensPositionSelectorsKt;
import io.fotoapparat.selector.ResolutionSelectorsKt;
import io.fotoapparat.selector.SelectorsKt;
import io.fotoapparat.view.CameraView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

public class ScreeningColorFragment extends Fragment {

    CameraView cameraView;
    Fotoapparat fotoapparat;
    FrameLayout layout;
    Button button;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Please note the third parameter should be false, otherwise a java.lang.IllegalStateException maybe thrown.
        final View retView = inflater.inflate(R.layout.fragment_screening_color, container, false);

        cameraView = retView.findViewById(R.id.camera_view);
        layout = retView.findViewById(R.id.totalFrame);
        button = retView.findViewById(R.id.submit_urinalysisResult);
        button.setOnClickListener(view -> {
            getResultString();
        });

        fotoapparat = Fotoapparat.with(getContext())
                .into(cameraView)           // view which will draw the camera preview
                .previewScaleType(ScaleType.CenterCrop)  // we want the preview to fill the view
                .photoResolution(ResolutionSelectorsKt.highestResolution())   // we want to have the biggest photo possible
                .lensPosition(LensPositionSelectorsKt.back())       // we want back camera
                .focusMode(SelectorsKt.firstAvailable(  // (optional) use the first focus mode which is supported by device
                        FocusModeSelectorsKt.continuousFocusPicture(),
                        FocusModeSelectorsKt.autoFocus(),        // in case if continuous focus is not available on device, auto focus will be used
                        FocusModeSelectorsKt.fixed()             // if even auto focus is not available - fixed focus mode will be used
                ))
                .flash(SelectorsKt.firstAvailable(      // (optional) similar to how it is done for focus mode, this time for flash
                        FlashSelectorsKt.autoRedEye(),
                        FlashSelectorsKt.autoFlash(),
                        FlashSelectorsKt.torch()
                ))
                //.frameProcessor(myFrameProcessor)   // (optional) receives each frame from preview stream
                .logger(LoggersKt.loggers(            // (optional) we want to log camera events in 2 places at once
                        LoggersKt.logcat(),           // ... in logcat
                        LoggersKt.fileLogger(getContext())    // ... and to file
                ))
                .build();

        retView.findViewById(R.id.capture).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PhotoResult photoResult = fotoapparat.takePicture();
                Toast.makeText(getContext(), "Screening.", Toast.LENGTH_LONG).show();
                ((TextView) retView.findViewById(R.id.report_text)).setTextColor(Color.RED);

                final ProgressDialog dialog = ProgressDialog.show(getContext(), "Processing Sample",
                        "Loading. Please wait...", true);

                fotoapparat.stop();
                Handler h = new Handler();

                h.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        dialog.dismiss();
                        button.setVisibility(View.VISIBLE);

                        /*SharedPreferences prefs = getActivity()
                                .getSharedPreferences(Constants.MY_PREFS_NAME, MODE_PRIVATE);
                        boolean showManual = prefs.getBoolean(Constants.SHOW_MANUAL, false);*/

                        /*if (MainActivity.volThres == 0 || MainActivity.volThres == -1) {
                            ScreeningColorPassFragment screeningColorPassFragment
                                    = new ScreeningColorPassFragment();
                            getActivity().getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.frame_container, screeningColorPassFragment)
                                    .addToBackStack("urine").commit();

                        } else {
                            ScreeningColorFailFragment screeningColorFailFragment
                                    = new ScreeningColorFailFragment();
                            getActivity().getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.frame_container, screeningColorFailFragment)
                                    .addToBackStack("urine").commit();
                        }*/

                    }
                }, 6000);

            }
        });

        return retView;
    }

    public void getResultString(){
        String capturedValues = "LeuKocytes:16,Nitrite:20,Urobilinogen:0.10,Protein:5,pH:6.000,Blood:4,SpecificGravity:1.030,Ketones:11,Bilirubin:13,Glucose:23";
        updateDoc(capturedValues);
    }

    private void updateDoc(String capturedValues) {
        try {
            File root = new File(getActivity().getExternalCacheDir(), "Notes");
            if (!root.exists()) {
                root.mkdirs();
            }
            File myObj = new File(root, "sFileName.txt");
            FileWriter writer = new FileWriter(myObj);
            writer.append(capturedValues);
            writer.flush();
            writer.close();

            RequestBody requestFile =
                    RequestBody.create(
                            MediaType.parse("text"),
                            myObj
                    );
            MultipartBody.Part body = MultipartBody.Part.createFormData("document",
                    myObj.getName(), requestFile);

            RequestBody date =
                    RequestBody.create(MediaType.parse("multipart/form-data"), "2021-01-23");
            RequestBody description =
                    RequestBody.create(MediaType.parse("multipart/form-data"), "Urinalysis Report");
            RequestBody patientId =
                    RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(91910191));
            RequestBody doctorId =
                    RequestBody.create(MediaType.parse("multipart/form-data"), "287938");


            SharedPreferences prefs = getActivity().getSharedPreferences(Constants.MY_PREFS_NAME,
                    MODE_PRIVATE);
            String token = prefs.getString(Constants.REFRESH_TOKEN, "");

            Call<ResponseBody> call = RetrofitBuilder.getInstance().create(RetrofitAPI.class)
                    .addRecord(token, 91910191, date, description,
                            patientId, doctorId, body);

            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()) {
                        ResponseBody responseBody = response.body();

                        Toast.makeText(getActivity(),
                                "Send reports to doctor to approve", Toast.LENGTH_LONG).show();
                    } else {
                        ResponseBody errorBody = response.errorBody();
                        Log.d("yug", "error failed" + errorBody.toString());
                        Gson gson = new Gson();

                        try {
                            Response errorResponse = gson.fromJson(errorBody.string(), Response.class);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.d("yug", "onFailure: " + t.getLocalizedMessage());
                }
            });
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        fotoapparat.start();
    }

    @Override
    public void onStop() {
        super.onStop();
        fotoapparat.stop();
    }

}
