package co.pocketnurse.Model;

public class PrescriptionModel {

    private String id ,doctor, patient, date, medication, disease;

    public PrescriptionModel(String id, String doctor, String patient, String date, String medication, String disease) {
        this.id = id;
        this.doctor = doctor;
        this.patient = patient;
        this.date = date;
        this.medication = medication;
        this.disease = disease;
    }

    public String getId() {
        return id;
    }

    public String getDoctor() {
        return doctor;
    }

    public String getPatient() {
        return patient;
    }

    public String getDate() {
        return date;
    }

    public String getMedication() {
        return medication;
    }

    public String getDisease() {
        return disease;
    }
}
