package co.pocketnurse.API;

import java.util.List;

import co.pocketnurse.Model.Medication;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface RetrofitAPI {
     /*91865832*/
    @GET("/api/medications")
    Call<List<Results>> getMedications(
             @Query("patient") int id,
             @Header("Authorization") String authHeader
    );

    String redirect_uri = "https://viralreplay.com/handler";
    String client_id = "xUUkLzrbW83MpLiiK0siq0rY5bBjiWFfEIKf8R1f";
    String client_secret = "6vKdL9b5LIbAKgthieHnSdFBqEyPBsfjuL1CTmjFTPVSbmZ7JgX0E4MpMpB48HzQeqDKUlrYpVFMi3HrkISkZSGyBaeEMZAUtlttLLnIPkWTywCQbDVRXKqUpafpW9Pw";
    String response_type = "code";
    String scope = "clinical:read patients:read patients:write clinical:write";

    //https://drchrono.com/o/authorize/?redirect_uri=REDIRECT_URI_ENCODED&response_type=code&client_id=CLIENT_ID_ENCODED&scope=SCOPES_ENCODED
    @FormUrlEncoded
    @POST("/o/authorize")
    Call doServerAuth(@Query("redirect_uri") String redirect_uri, @Query("client_id") String client_id,
                      @Query("response_type") String response_type, @Query("scope") String scope);

    //https://drchrono.com/o/token/
    @FormUrlEncoded
    @POST("/o/token")
    Call<ResponseBody> getToken(@Field("refresh_token") String refresh_token);

    // https://app.drchrono.com/api/medications?patient=91865832
    @GET("/api/medications")
    Call<Medication> getMedications(@Header("Authorization") String authorization, @Query("patient") int id);


    //https://app.drchrono.com/api/documents?patient=91865832
    @Multipart
    @POST("/api/documents")
    Call<ResponseBody> addRecord(@Header("Authorization") String authorization,
                                 @Query("patient") int patient,
                                 @Part("date") RequestBody date,
                                 @Part("description") RequestBody description,
                                 @Part("patient") RequestBody patientId,
                                 @Part("doctor") RequestBody doctor,
                                 @Part MultipartBody.Part document);

}
