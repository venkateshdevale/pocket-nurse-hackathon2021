package co.pocketnurse.API;

import com.google.gson.annotations.SerializedName;

public class Results {


    @SerializedName("username")
    String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
