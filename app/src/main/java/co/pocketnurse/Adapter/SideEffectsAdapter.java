package co.pocketnurse.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import co.pocketnurse.R;

import java.util.ArrayList;

public class SideEffectsAdapter extends RecyclerView.Adapter<SideEffectsAdapter.ViewHolder> {


    private ArrayList<String> sideEffectsList;
    private ArrayList<String> selectedSideEffectsList = new ArrayList<>();
    private Context context;

    public SideEffectsAdapter(Context mContext, ArrayList<String> mSideEffectsList) {

        this.sideEffectsList = mSideEffectsList;
        this.context = mContext;

    }


    @NonNull
    @Override
    public SideEffectsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SideEffectsAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_side_effect_adapter, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull SideEffectsAdapter.ViewHolder holder, final int position) {


        final Object object = sideEffectsList.get(position);
        holder.name.setText(object.toString());



    }

    @Override
    public int getItemCount() {
        return sideEffectsList.size();
    }

    public ArrayList<String> returnList(){
        return selectedSideEffectsList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        CheckBox checkBox;


        public ViewHolder(@NonNull final View itemView) {

            super(itemView);

            name = itemView.findViewById(R.id.side_effect_name);
            checkBox = itemView.findViewById(R.id.checkBox_sideEffects);


            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                    if (isChecked) {
                        selectedSideEffectsList.add(sideEffectsList.get(getAbsoluteAdapterPosition()));
                    }else {
                        selectedSideEffectsList.remove(sideEffectsList.get(getAbsoluteAdapterPosition()));
                    }
                }
            });

        }
    }
}
