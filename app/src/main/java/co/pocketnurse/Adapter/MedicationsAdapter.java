package co.pocketnurse.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import co.pocketnurse.BottomSheet.Detail_Medicine_Sheet;
import co.pocketnurse.R;

import java.util.ArrayList;

public class MedicationsAdapter extends RecyclerView.Adapter<MedicationsAdapter.ViewHolder> {


    private ArrayList<String> medicationsList;
    private Context context;

    public MedicationsAdapter(Context mContext, ArrayList<String> mMedicationsList) {

        this.medicationsList = mMedicationsList;
        this.context = mContext;

    }


    @NonNull
    @Override
    public MedicationsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MedicationsAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_medications_adapter, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MedicationsAdapter.ViewHolder holder, final int position) {


        final Object object = medicationsList.get(position);
        holder.name.setText(object.toString());



    }

    @Override
    public int getItemCount() {
        return medicationsList.size();
    }

    public ArrayList<String> returnList(){
        return medicationsList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView name;


        public ViewHolder(@NonNull final View itemView) {

            super(itemView);

            name = itemView.findViewById(R.id.medication_name);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Detail_Medicine_Sheet medicine_sheet = new Detail_Medicine_Sheet(name.getText().toString());
                    medicine_sheet.setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.BottomSheetTheme);
                    medicine_sheet.show(((AppCompatActivity)context).getSupportFragmentManager(), "BottomSheet");
                }
            });

        }
    }
}
