package co.pocketnurse.Adapter;

import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import co.pocketnurse.DetailedPrescriptionActivity;
import co.pocketnurse.Model.PrescriptionModel;
import co.pocketnurse.R;

import java.util.ArrayList;

public class PrescriptionsAdapter extends RecyclerView.Adapter<PrescriptionsAdapter.ViewHolder> {

    private ArrayList<PrescriptionModel> containsList;
    private Context context;

    public PrescriptionsAdapter(Context mContext, ArrayList<PrescriptionModel> mContainsList) {

        this.containsList = mContainsList;
        this.context = mContext;

    }

    @NonNull
    @Override
    public PrescriptionsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PrescriptionsAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_prescription_adapter, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull PrescriptionsAdapter.ViewHolder holder, final int position) {

        PrescriptionModel model = containsList.get(position);
        holder.doctor_name.setText(model.getDoctor());
        holder.date.setText(model.getDate());
        holder.prescription_header.setText("Prescription for " + model.getDisease());
        holder.medication.setText(Html.fromHtml("" + model.getMedication().replace(",", "<br>") ));


    }

    @Override
    public int getItemCount() {
        return containsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView doctor_name, date, medication, prescription_header;


        public ViewHolder(@NonNull final View itemView) {

            super(itemView);

            doctor_name = itemView.findViewById(R.id.doctor_name);
            date = itemView.findViewById(R.id.date);
            medication = itemView.findViewById(R.id.medication_name_main);
            prescription_header = itemView.findViewById(R.id.header_medication);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(itemView.getContext(), DetailedPrescriptionActivity.class);
                    intent.putExtra("doctor", containsList.get(getAbsoluteAdapterPosition()).getDoctor());
                    intent.putExtra("date", containsList.get(getAbsoluteAdapterPosition()).getDate());
                    intent.putExtra("medications", containsList.get(getAbsoluteAdapterPosition()).getMedication());
                    intent.putExtra("disease", containsList.get(getAbsoluteAdapterPosition()).getDisease());
                    itemView.getContext().startActivity(intent);
                }
            });

        }
    }
}
