package co.pocketnurse.Fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import co.pocketnurse.Adapter.PrescriptionsAdapter;
import co.pocketnurse.Model.PrescriptionModel;
import co.pocketnurse.R;
import co.pocketnurse.databinding.FragmentPrescriptionBinding;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;


public class PrescriptionFragment extends Fragment {

    FragmentPrescriptionBinding binding;
    private ArrayList<PrescriptionModel> mContainsList;
    private PrescriptionsAdapter mAdapter;
    private String TAG = "PrescriptionFragment";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentPrescriptionBinding.inflate(getLayoutInflater(), container, false);


        mContainsList = new ArrayList<>();
        binding.recyclerViewPrescriptions.setHasFixedSize(true);
        binding.recyclerViewPrescriptions.setLayoutManager(new LinearLayoutManager(getContext()));
        addItemsFromJSON();
        mAdapter = new PrescriptionsAdapter(getContext(), mContainsList);
        binding.recyclerViewPrescriptions.setAdapter(mAdapter);



        return binding.getRoot();
    }

    private void addItemsFromJSON() {
        try {

            String jsonDataString = readJSONDataFromFile();
            JSONObject object = new JSONObject(jsonDataString);
            JSONArray jsonArray  = object.getJSONArray("results");

            for (int i=0; i<jsonArray.length(); ++i) {

                JSONObject itemObj = jsonArray.getJSONObject(i);

                String id = itemObj.getString("id");
                String doctor = itemObj.getString("doctor");
                String patient = itemObj.getString("patient");
                String date = itemObj.getString("date_prescribed");
                String medication = itemObj.getString("prescriptions");
                String disease = itemObj.getString("disease");

                PrescriptionModel model = new PrescriptionModel(id, doctor, patient, date, medication, disease);
                mContainsList.add(model);


            }

        } catch (JSONException | IOException e) {
            Log.e(TAG, "Error: " + e.getMessage());
        }
    }

    private String readJSONDataFromFile() throws IOException{

        InputStream inputStream = null;
        StringBuilder builder = new StringBuilder();

        try {

            String jsonString = null;
            inputStream = getResources().openRawResource(R.raw.sample);
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(inputStream, "UTF-8"));

            while ((jsonString = bufferedReader.readLine()) != null) {
                builder.append(jsonString);
            }

        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
        return new String(builder);
    }

}