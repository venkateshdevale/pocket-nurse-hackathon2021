package co.pocketnurse.Fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import co.pocketnurse.HeartRateCheck.Home;
import co.pocketnurse.Urinalysis.UrinalysisMainActivity;
import co.pocketnurse.databinding.FragmentCheckupBinding;

public class CheckUpFragment extends Fragment {


    FragmentCheckupBinding binding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        binding = FragmentCheckupBinding.inflate(inflater, container, false);


        binding.checkHeartRateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), Home.class));
            }
        });
        binding.uninalysisBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), UrinalysisMainActivity.class));
            }
        });


        return binding.getRoot();
    }
}