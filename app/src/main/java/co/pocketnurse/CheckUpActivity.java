package co.pocketnurse;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import co.pocketnurse.HeartRateCheck.Home;
import co.pocketnurse.Urinalysis.UrinalysisMainActivity;
import co.pocketnurse.databinding.ActivityCheckUpBinding;

public class CheckUpActivity extends AppCompatActivity {

    ActivityCheckUpBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityCheckUpBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);


        binding.checkHeartRateBtn.setOnClickListener(view1 -> startActivity(new Intent(CheckUpActivity.this, Home.class)));
        binding.uninalysisBtn.setOnClickListener(view12 -> startActivity(new Intent(CheckUpActivity.this, UrinalysisMainActivity.class)));

    }
}