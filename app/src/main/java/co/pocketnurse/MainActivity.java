package co.pocketnurse;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.fxn.OnBubbleClickListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import co.pocketnurse.API.RetrofitAPI;
import co.pocketnurse.API.RetrofitBuilder;
import co.pocketnurse.Fragments.CheckUpFragment;
import co.pocketnurse.Fragments.PrescriptionFragment;
import co.pocketnurse.R;
import co.pocketnurse.databinding.ActivityMainBinding;
import co.pocketnurse.managers.Constants;

public class MainActivity extends AppCompatActivity {

    ActivityMainBinding binding;
    int current_id;

    private String clientID = "Pw1S4SfNNtOLbO0xYeIDRHEu4tZHveGOzg5PtiAR";
    private String clientSecret = "h2IQUuMY4wnKE9u00bTIZ3UBzsJrzZthZelhKHcFfjllfeFM3y7I5UrNOWsOwPObOzEuJgtRkYyFpDdrzRS146HyrQklsp8U2S2xjmaRhhahY8I4CCjTmKjzOD4JMSQS";
    private String redirectURL = "https://com.jaikeerthick.medicalprescription:/oauth2redirect";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        getWindow().setNavigationBarColor(getResources().getColor(R.color.colorPrimary));

        doRefresh();

        getSupportFragmentManager().beginTransaction().replace(R.id.container,
                new PrescriptionFragment()).commit();
        current_id = R.id.nav_prescription;


        binding.bubbleTabBar.addBubbleListener(new OnBubbleClickListener() {
            @Override
            public void onBubbleClick(int i) {

                if (current_id!=i) {

                    Fragment selectedFragment = null;

                    switch (i) {
                        case R.id.nav_prescription:
                            selectedFragment = new PrescriptionFragment();
                            binding.header.setText("Prescriptions");
                            current_id = i;
                            break;
                        case R.id.nav_checkUp:
                            selectedFragment = new CheckUpFragment();
                            binding.header.setText("Checkup");
                            current_id = i;
                            break;
                        case R.id.nav_carePlan:
                            selectedFragment = new PrescriptionFragment();
                            binding.header.setText("Prescriptions");
                            current_id = i;
                            break;

                    }

                    assert selectedFragment != null;
                    getSupportFragmentManager().beginTransaction().replace(R.id.container,
                            selectedFragment).commit();
                }

            }
        });
    }


    @Override
    protected void onStart() {
        super.onStart();

        int id = 91865832;

        RetrofitAPI service = RetrofitBuilder.getInstance().create(RetrofitAPI.class);

       /* Call<Results> call = service.getMedications(id, "Bearer " + "cn3odwSvUdLMzVSY66cLgQUHVRSCBv");

        call.enqueue(new Callback<Results>() {
            @Override
            public void onResponse(Call<Results> call, Response<Results> response) {


                if (!response.isSuccessful()){
                    Toast.makeText(MainActivity.this, "Error Code:" + response.code(), Toast.LENGTH_SHORT).show();
                    return;
                }
                Results results = response.body();
                *//*Toast.makeText(MainActivity.this, results.getUsername(), Toast.LENGTH_SHORT).show();*//*

            }

            @Override
            public void onFailure(Call<Results> call, Throwable t) {

                Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });*/


    }

    private void doRefresh() {
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("refresh_token")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d("yug", document.getId() + " => " + document.getData());

                                String refresh_token = (String) document.getData().get("refresh_token");
                                Log.d("yug", "ref: " + refresh_token);
                                SharedPreferences.Editor editor = getSharedPreferences(Constants.MY_PREFS_NAME,
                                        MODE_PRIVATE).edit();
                                editor.putBoolean(Constants.IS_SKIP, true);
                                editor.putString(Constants.REFRESH_TOKEN, refresh_token);
                                editor.apply();
                                break;
                            }
                        } else {
                            Log.d("yug", "Error getting documents.", task.getException());
                        }
                    }
                });
    }


}