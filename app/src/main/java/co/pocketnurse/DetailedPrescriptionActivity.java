package co.pocketnurse;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Toast;

import co.pocketnurse.Adapter.MedicationsAdapter;
import co.pocketnurse.databinding.ActivityDetailedPrescriptionBinding;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

public class DetailedPrescriptionActivity extends AppCompatActivity {

    ActivityDetailedPrescriptionBinding binding;
    SharedPreferences preferences;
    ArrayList<String> list = new ArrayList<>();
    private MedicationsAdapter mAdapter;
    String medications;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityDetailedPrescriptionBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        setSupportActionBar(binding.toolbarDetail);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        String doctor = getIntent().getStringExtra("doctor");
        String date = getIntent().getStringExtra("date");
        String disease = getIntent().getStringExtra("disease");
        medications = getIntent().getStringExtra("medications");

        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        binding.doctorName.setText(doctor);
        binding.date.setText(date);
        binding.headerMedicationDetail.setText("Prescription for " + disease);

        setupList();
        showList();

        binding.reportEffectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveSideEffects();
            }
        });

    }

    public void setupList(){

      String[] medicationsArray = medications.split(",");
        list.addAll(Arrays.asList(medicationsArray));

    }
    public void showList(){

        binding.recyclerViewMedications.setHasFixedSize(true);
        binding.recyclerViewMedications.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new MedicationsAdapter(this, list);
        binding.recyclerViewMedications.setAdapter(mAdapter);

    }

    public void saveSideEffects(){

        String selectedSideEffects = preferences.getString("selectedSideEffects", "");
        if (selectedSideEffects.isEmpty()){
            Toast.makeText(this, "Please select some side effects", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(this, selectedSideEffects, Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onDestroy() {
        preferences.edit().remove("selectedSideEffects").apply();
        super.onDestroy();
    }
}